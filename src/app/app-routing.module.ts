import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeroesComponent } from './heroes/heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';

import { MaterialAngularComponent } from './material-angular/material-angular.component';
import {AutocompleteDesComponent} from './autocomplete-des/autocomplete-des.component';
import {SelectionListComponent} from './selection-list/selection-list.component';
import { EstudosComponent } from './estudos/estudos.component';
import { FormControlComponent } from './form-control/form-control.component';


const routes: Routes = [
  { path: 'heroes', component: HeroesComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'material-angular', component: MaterialAngularComponent },
  { path: 'autocomplete', component: AutocompleteDesComponent },
  { path: 'list', component: SelectionListComponent },
  { path: 'estudos', component: EstudosComponent },
  { path: 'form-control', component: FormControlComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }