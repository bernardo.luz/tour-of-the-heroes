import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteDesComponent } from './autocomplete-des.component';

describe('AutocompleteDesComponent', () => {
  let component: AutocompleteDesComponent;
  let fixture: ComponentFixture<AutocompleteDesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteDesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteDesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
