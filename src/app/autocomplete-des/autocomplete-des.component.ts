import { Component, OnInit } from '@angular/core';
import {MessageService} from '../message.service';
import {Hero} from '../hero'
import {FormControl} from '@angular/forms';
import {Subject, Observable} from 'rxjs';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { HeroService } from '../hero.service';



@Component({
  selector: 'app-autocomplete-des',
  templateUrl: './autocomplete-des.component.html',
  styleUrls: ['./autocomplete-des.component.css']
})
export class AutocompleteDesComponent implements OnInit {
  myControl = new FormControl();
  heroes$: Observable<Hero[]>;
  private searchTerms = new Subject<string>();
  


  constructor(private messageService: MessageService, private heroService: HeroService) {
    messageService.add('Entered autocomplete page!');
  }
  search(term: string): void {
    this.searchTerms.next(term);
  }
  
  
  ngOnInit(): void {
    this.heroes$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.heroService.searchHeroes(term)),
    );
    }
 

}
