import { Component, OnInit, NgModule, Input } from '@angular/core';
import {MessageService} from '../message.service';
import {Hero} from '../hero'
import {HEROES} from '../mock-heroes'

@Component({
  selector: 'app-material-angular',
  templateUrl: './material-angular.component.html',
  styleUrls: ['./material-angular.component.css'],
})

export class MaterialAngularComponent  implements OnInit {
  @Input() clicked: boolean
  heroes:Hero[];
  

  constructor(private messageService: MessageService) { 
    this.clicked=false;
    messageService.add('entered material angular page!');
  }

  ngOnInit(): void {
  }

  mostrarHeroes(){
    this.heroes = HEROES;
    this.clicked=true;
    
  }
  esconderHeroes(){
    this.heroes = [];
    this.clicked=false;
  }

}
